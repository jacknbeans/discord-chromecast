const Discord = require('discord.js');
const client = new Discord.Client();
const rest = require('rest');

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', msg => {
  if(msg.content.startsWith('!video')) {
    var args = msg.content.split(' ');
    if(args.length < 2) {
      msg.channel.send('I could not find a video URL in the arguments, please try again with the format \'!video <video_url>\'');
      return;
    }

    var videoUrl = args[1];
    rest({
      path: 'http://localhost:5000/api/videos',
      entity: videoUrl
    }).then(response => {
      if(response.status.code !== 200) {
        msg.channel.send(`Could not queue video\ncode: ${response.status.code}\ntext: ${response.status.text}`);
        return;
      }

      msg.channel.send(`Queued video ending in ${response.entity}`);
    }).catch(error => {
      console.error(error);
      msg.channel.send(`I heard you, but I ran into an error while making your request. Please, try again later!`);
    });
  } else if(msg.content.startsWith('!skip')) {
    rest({
      method: 'POST',
      path: 'http://localhost:5000/api/skip'
    }).then(response => {
      if(response.status.code !== 200) {
        msg.channel.send(`Could not skip video\ncode: ${response.status.code}\ntext: ${response.status.text}`);
        return;
      }

      msg.channel.send(`Skipped video!`);
    }).catch(error => {
      console.error(error);
      msg.channel.send(`I heard you, but I ran into an error while making your request. Please, try again later!`);
    });
  }
});

client.login(process.env.BOT_TOKEN);